### Client-server connection

##### Run server:
* for run server, need input in terminal next command
```bash
./example-004-client-server-connection/server.py
```
* run client and receive response from server in terminal
```bash
./example-004-client-server-connection/client.py
```
* for stop server, need input in terminal next `CTRL+C` (Linux)

##### Resources:
* [Write a simple HTTP server in Python](https://www.acmesystems.it/python_http)
* [What is the quickest way to HTTP GET in Python?](https://stackoverflow.com/questions/645312/what-is-the-quickest-way-to-http-get-in-python)

