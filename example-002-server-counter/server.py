#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

count = 0


# This class will handles any incoming request from
# the browser
class CustomHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        # Send the message

        # increment counter
        global count
        count += 1

        self.wfile.write("counter = " + str(count))
        return


try:
    PORT_NUMBER = 8080
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), CustomHandler)

    print 'Started http server on port ', PORT_NUMBER

    # Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'

    server.socket.close()
