### Counter Python server

##### Run server:
* for run server, need input in terminal next command
```bash
./example-002-server-counter/server.py
```
* for see result, open in browser: [localhost:8080](http://localhost:8080/)
* refresh page in browser: [localhost:8080](http://localhost:8080/) or `CTRL+R` or `F5` and see counter change
* you see increment +2, because every time receive 2 request "GET / HTTP/1.1" & "GET /favicon.ico HTTP/1.1"
* for stop server, need input in terminal next `CTRL+C` (Linux)

##### Resources:
* [Write a simple HTTP server in Python](https://www.acmesystems.it/python_http)
* [Global, Local and nonlocal Variables](https://www.python-course.eu/python3_global_vs_local_variables.php)
