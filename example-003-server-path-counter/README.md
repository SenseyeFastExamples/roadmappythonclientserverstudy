### Counter by pages Python server

##### Run server:
* for run server, need input in terminal next command
```bash
./example-003-server-path-counter/server.py
```
* for see result, open in browser: [localhost:8080](http://localhost:8080/)
* refresh page in browser: [localhost:8080](http://localhost:8080/) or `CTRL+R` or `F5` and see counter change
* for see stats by pages, go to [localhost:8080/stats](http://localhost:8080/stats)
* for stop server, need input in terminal next `CTRL+C` (Linux)

##### Resources:
* [Write a simple HTTP server in Python](https://www.acmesystems.it/python_http)
* [Global, Local and nonlocal Variables](https://www.python-course.eu/python3_global_vs_local_variables.php)
* [Python — Dictionary](https://www.tutorialspoint.com/python/python_dictionary.htm)
* [Iterating over dictionaries using 'for' loops](https://stackoverflow.com/questions/3294889/iterating-over-dictionaries-using-for-loops)
* [Словари (dict) и работа с ними. Методы словарей](https://pythonworld.ru/tipy-dannyx-v-python/slovari-dict-funkcii-i-metody-slovarej.html)
