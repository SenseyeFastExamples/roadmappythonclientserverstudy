### Client-server last message

##### Run server:
* for run server, need input in terminal next command
```bash
./example-005-client-last-message/server.py
```
* run client in terminal and send message to server
```bash
./example-005-client-last-message/client.py send hello
```
* run client in terminal and read last message
```bash
./example-005-client-last-message/client.py read
```
* for stop server, need input in terminal next `CTRL+C` (Linux)

##### Resources:
* [Write a simple HTTP server in Python](https://www.acmesystems.it/python_http)
* [Global, Local and nonlocal Variables](https://www.python-course.eu/python3_global_vs_local_variables.php)
* [What is the quickest way to HTTP GET in Python?](https://stackoverflow.com/questions/645312/what-is-the-quickest-way-to-http-get-in-python)
* [Python — Command Line Arguments](https://www.tutorialspoint.com/python/python_command_line_arguments.htm)
* [Making a POST call instead of GET using urllib2](https://stackoverflow.com/questions/6348499/making-a-post-call-instead-of-get-using-urllib2)
* [Как получить доступ к данным, отправленным на мой сервер с помощью BaseHTTPRequestHandler?](http://qaru.site/questions/949622/how-do-i-access-the-data-sent-to-my-server-using-basehttprequesthandler)
