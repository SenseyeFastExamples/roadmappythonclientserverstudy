#!/usr/bin/env python

import urllib2
import sys

# first argument it always name of started script, in this case = "./example-005-client-server-last-message/client.py"
# if want see
# print str(sys.argv)

if len(sys.argv) < 2:
    print("please do any action of send or read")
    print("example: `send {some message}`, `send hello`, to send your message to server and store")
    print("example: `read`, and receive last message")
else:
    action = sys.argv[1]

    if action == "read":
        contents = urllib2.urlopen('http://localhost:8080/').read()

        print(contents)
    elif action == "send":
        if len(sys.argv) > 2:
            message = sys.argv[2]

            url = 'http://localhost:8080/'
            req = urllib2.Request(url, message)
            response = urllib2.urlopen(req)
            print response.read()
        else:
            print("please input message to send")
    else:
        print("Incorrect action", action)
