### Client-server last message from stack

##### Run server:
* for run server, need input in terminal next command
```bash
./example-006-client-server-stack/server.py
```
* run client in terminal and send message to server
```bash
./example-006-client-server-stack/client.py send hello
```
* run client in terminal and read last message
```bash
./example-006-client-server-stack/client.py read
```
* for stop server, need input in terminal next `CTRL+C` (Linux)

##### Resources:
* [Write a simple HTTP server in Python](https://www.acmesystems.it/python_http)
* [Global, Local and nonlocal Variables](https://www.python-course.eu/python3_global_vs_local_variables.php)
* [What is the quickest way to HTTP GET in Python?](https://stackoverflow.com/questions/645312/what-is-the-quickest-way-to-http-get-in-python)
* [Python — Command Line Arguments](https://www.tutorialspoint.com/python/python_command_line_arguments.htm)
* [Making a POST call instead of GET using urllib2](https://stackoverflow.com/questions/6348499/making-a-post-call-instead-of-get-using-urllib2)
* [Как получить доступ к данным, отправленным на мой сервер с помощью BaseHTTPRequestHandler?](http://qaru.site/questions/949622/how-do-i-access-the-data-sent-to-my-server-using-basehttprequesthandler)
* [Implementing a Stack in Python](http://interactivepython.org/courselib/static/pythonds/BasicDS/ImplementingaStackinPython.html)
