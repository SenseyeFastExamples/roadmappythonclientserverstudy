#!/usr/bin/python
import unittest
import subprocess


class TestStringMethods(unittest.TestCase):

    def test_client_help(self):
        process = subprocess.Popen(
            # in Windows try some like C:\...\python*.exe
            ['python ./example-009-client-server-chat-rooms-testings/client.py -h'],
            stdout=subprocess.PIPE,
            shell=True,
        )

        actual = process.stdout.read()
        expect = """usage: client.py [-h] [-p P] [-r R] [-m M] action

positional arguments:
  action      action read or send

optional arguments:
  -h, --help  show this help message and exit
  -p P        PORT for server connection
  -r R        chat room name
  -m M        message
"""

        self.assertEqual(expect, actual)

unittest.main()
