### Client-server last message from chat room with testing

##### Run server:
* for run server, need input in terminal next command with flags
```bash
./example-009-client-server-chat-rooms-testings/server.py -p 8000
```
* run client in terminal and send message to server
```bash
./example-009-client-server-chat-rooms-testings/client.py send -r greatingroom -m hello
```
* run client in terminal and read last message
```bash
./example-009-client-server-chat-rooms-testings/client.py read -r greatingroom
```
* for stop server, need input in terminal next `CTRL+C` (Linux)
* for run tests, need input in terminal next command
```bash
./example-009-client-server-chat-rooms-testings/client_test.py
```

##### Resources:
* [Write a simple HTTP server in Python](https://www.acmesystems.it/python_http)
* [Global, Local and nonlocal Variables](https://www.python-course.eu/python3_global_vs_local_variables.php)
* [What is the quickest way to HTTP GET in Python?](https://stackoverflow.com/questions/645312/what-is-the-quickest-way-to-http-get-in-python)
* [Python — Command Line Arguments](https://www.tutorialspoint.com/python/python_command_line_arguments.htm)
* [Making a POST call instead of GET using urllib2](https://stackoverflow.com/questions/6348499/making-a-post-call-instead-of-get-using-urllib2)
* [Как получить доступ к данным, отправленным на мой сервер с помощью BaseHTTPRequestHandler?](http://qaru.site/questions/949622/how-do-i-access-the-data-sent-to-my-server-using-basehttprequesthandler)
* [Implementing a Stack in Python](http://interactivepython.org/courselib/static/pythonds/BasicDS/ImplementingaStackinPython.html)
* [Parser for command-line options, arguments and sub-commands](https://docs.python.org/2/library/argparse.html)
* [sprintf like functionality in Python](https://stackoverflow.com/questions/5309978/sprintf-like-functionality-in-python)
* [String Formatting Operations](https://docs.python.org/2/library/stdtypes.html#string-formatting-operations)
* [unittest — Unit testing framework](https://docs.python.org/3/library/unittest.html)
* [How to get output from subprocess.Popen(). proc.stdout.readline() blocks, no data prints out](https://stackoverflow.com/questions/1388753/how-to-get-output-from-subprocess-popen-proc-stdout-readline-blocks-no-dat)