#!/usr/bin/env python

import urllib2
import sys

# first argument it always name of started script, in this case = "./example-007-client-server-chat-rooms/client.py"
# if want see
# print str(sys.argv)

if len(sys.argv) < 3:
    print("please do any action of send or read")
    print("example: `send {room} {some message}`, `send greating hello`, to send your message to server and store")
    print("example: `read {room}`, `read {room}`, and receive last message from {room}")
else:
    action = sys.argv[1]
    room = sys.argv[2]

    path = 'http://localhost:8080/' + room

    if action == "read":
        contents = urllib2.urlopen(path).read()

        print(contents)
    elif action == "send":
        if len(sys.argv) > 3:
            message = sys.argv[3]

            url = path
            req = urllib2.Request(url, message)
            response = urllib2.urlopen(req)
            print response.read()
        else:
            print("please input message to send")
    else:
        print("Incorrect action", action)
