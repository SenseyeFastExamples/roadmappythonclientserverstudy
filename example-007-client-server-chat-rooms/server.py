#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

chatRoomMessagesDictionary = {}


# This class will handles any incoming request from
# the browser
class CustomHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        # Send the message

        chatRoom = self.path

        global chatRoomMessagesDictionary

        if chatRoom in chatRoomMessagesDictionary:
            messages = chatRoomMessagesDictionary[chatRoom]

            if len(messages) > 0:
                self.wfile.write(messages.pop())
            else:
                self.wfile.write("You already read all messages from chatroom")
        else:
            self.wfile.write("Undefined chatroom")

        return

    def do_POST(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

        chatRoom = self.path

        self.wfile.write("Success")

        length = int(self.headers['Content-Length'])
        global chatRoomMessagesDictionary

        message = self.rfile.read(length).decode('utf-8')

        if chatRoom in chatRoomMessagesDictionary:
            messages = chatRoomMessagesDictionary[chatRoom]

            messages.append(message)
        else:
            chatRoomMessagesDictionary[chatRoom] = [message]

        return


try:
    PORT_NUMBER = 8080
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), CustomHandler)

    print 'Started http server on port ', PORT_NUMBER

    # Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'

    server.socket.close()
