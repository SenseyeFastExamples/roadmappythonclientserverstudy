#!/usr/bin/env python

import urllib2
import sys
from argparse import ArgumentParser

# first argument it always name of started script, in this case = "./example-008-client-server-chat-rooms-settings/client.py"
# if want see

parser = ArgumentParser()
parser.add_argument('action', type=str, help='action read or send')
parser.add_argument('-p', type=int, default=8000, help='PORT for server connection')
parser.add_argument('-r', type=str, default='', help='chat room name')
parser.add_argument('-m', type=str, default='', help='message')
args = parser.parse_args()

room = args.r
if len(room) == 0:
    print("please do any action of send or read")
    print("example: `send -r {room} -m {some message}`, `send -r greatingroom -m hello`, to send your message to server and store")
    print("example: `read -r {room}`, `read -r greatingroom`, and receive last message from {room}")
else:
    action = args.action

    # fetch argument "-p"
    PORT_NUMBER = args.p

    SERVER_URL = 'http://localhost:%d/' % PORT_NUMBER

    path = SERVER_URL + room

    if action == "read":
        contents = urllib2.urlopen(path).read()

        print(contents)
    elif action == "send":
        message = args.m
        if len(message) > 0:
            req = urllib2.Request(path, message)
            response = urllib2.urlopen(req)
            print response.read()
        else:
            print("please input message to send")
    else:
        print("Incorrect action", action)
