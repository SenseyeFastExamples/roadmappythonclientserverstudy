### Hello world Python server

##### Run server:
* for run server, need input in terminal next command
```bash
./example-001-server-hello-world/server.py
```
* for see result, open in browser: [localhost:8080](http://localhost:8080/)
* for stop server, need input in terminal next `CTRL+C` (Linux)

##### Resources:
* [Write a simple HTTP server in Python](https://www.acmesystems.it/python_http)
